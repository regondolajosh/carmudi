

Mobile Development Test App for Carmudi Philippines

IDE used:

Android Studio

Language used:
Java

Libraries used:

Glide - Image loader
RxJava
Butterknife - View injection
Retrofit - Network library
