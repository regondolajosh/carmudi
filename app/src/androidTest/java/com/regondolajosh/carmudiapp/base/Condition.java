package com.regondolajosh.carmudiapp.base;


public interface Condition {
    boolean isSatisfied();
}
