package com.regondolajosh.carmudiapp;

import android.support.test.runner.AndroidJUnit4;

import com.regondolajosh.carmudiapp.base.AbstractTest;
import com.regondolajosh.carmudiapp.contract.MainContract;
import com.regondolajosh.carmudiapp.manager.HttpManager;
import com.regondolajosh.carmudiapp.model.GetAdsListResponseModel;
import com.regondolajosh.carmudiapp.network.GetAdsListService;
import com.regondolajosh.carmudiapp.presenter.MainActivityPresenter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;

import rx.Observable;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Rego on 12/1/18.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityPresenterTest extends AbstractTest {

    @Test
    public void testAttach() {
        MainActivityPresenter mainActivityPresenter = new MainActivityPresenter();
        assertNull(mainActivityPresenter.view);

        mainActivityPresenter.attach(mock(MainContract.View.class));
        assertNotNull(mainActivityPresenter.view);

    }

    @Test
    public void testDetach() {
        MainActivityPresenter mainActivityPresenter = new MainActivityPresenter();
        mainActivityPresenter.attach(mock(MainContract.View.class));
        assertNotNull(mainActivityPresenter.view);

        mainActivityPresenter.detach();
        assertNull(mainActivityPresenter.view);

    }

    @Test void testLoadData() {
        MainActivityPresenter mainActivityPresenter = new MainActivityPresenter();
        mainActivityPresenter.attach(mock(MainContract.View.class));

        GetAdsListService getAdsListService = Mockito.mock(GetAdsListService.class);
        HttpManager.getInstance().setGetAdsListService(getAdsListService);


        //Test ok response
        GetAdsListResponseModel getAdsListResponseModel = new GetAdsListResponseModel();
        when(getAdsListService.getAdsList(1,10, "")).thenReturn(Observable.just(getAdsListResponseModel));
        waitFor(50);
    }

}
