package com.regondolajosh.carmudiapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Rego on 12/1/18.
 */

public final class NetworkManager {

    public static boolean isConnected(Context context) {

        ConnectivityManager ConnectionManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected() == true) {
            return true;

        } else {
            return false;
        }
    }


}
