package com.regondolajosh.carmudiapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceAttributes implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PriceAttributes> CREATOR = new Parcelable.Creator<PriceAttributes>() {
        @Override
        public PriceAttributes createFromParcel(Parcel in) {
            return new PriceAttributes(in);
        }

        @Override
        public PriceAttributes[] newArray(int size) {
            return new PriceAttributes[size];
        }
    };
    @SerializedName("price_conditions")
    @Expose
    private String priceConditions;

    protected PriceAttributes(Parcel in) {
        priceConditions = in.readString();
    }

    public String getPriceConditions() {
        return priceConditions;
    }

    public void setPriceConditions(String priceConditions) {
        this.priceConditions = priceConditions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(priceConditions);
    }
}