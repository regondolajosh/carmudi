package com.regondolajosh.carmudiapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Simples implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Simples> CREATOR = new Parcelable.Creator<Simples>() {
        @Override
        public Simples createFromParcel(Parcel in) {
            return new Simples(in);
        }

        @Override
        public Simples[] newArray(int size) {
            return new Simples[size];
        }
    };
    @SerializedName("CarId")
    @Expose
    private CarId tO002CAAAQ501INTCARID;

    protected Simples(Parcel in) {
        tO002CAAAQ501INTCARID = (CarId) in.readValue(CarId.class.getClassLoader());
    }

    public CarId getTO002CAAAQ501INTCARID() {
        return tO002CAAAQ501INTCARID;
    }

    public void setTO002CAAAQ501INTCARID(CarId tO002CAAAQ501INTCARID) {
        this.tO002CAAAQ501INTCARID = tO002CAAAQ501INTCARID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(tO002CAAAQ501INTCARID);
    }
}