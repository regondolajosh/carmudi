package com.regondolajosh.carmudiapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class All implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<All> CREATOR = new Parcelable.Creator<All>() {
        @Override
        public All createFromParcel(Parcel in) {
            return new All(in);
        }

        @Override
        public All[] newArray(int size) {
            return new All[size];
        }
    };
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;
    @SerializedName("optional")
    @Expose
    private List<Optional> optional = null;

    protected All(Parcel in) {
        if (in.readByte() == 0x01) {
            details = new ArrayList<>();
            in.readList(details, Detail.class.getClassLoader());
        } else {
            details = null;
        }
        if (in.readByte() == 0x01) {
            optional = new ArrayList<Optional>();
            in.readList(optional, Optional.class.getClassLoader());
        } else {
            optional = null;
        }
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public List<Optional> getOptional() {
        return optional;
    }

    public void setOptional(List<Optional> optional) {
        this.optional = optional;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (details == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(details);
        }
        if (optional == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(optional);
        }
    }
}