package com.regondolajosh.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

    @SerializedName("price_not_available")
    @Expose
    private String priceNotAvailable;
    @SerializedName("original_price_currency")
    @Expose
    private String originalPriceCurrency;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("original_price")
    @Expose
    private String originalPrice;
    @SerializedName("price_conditions_position")
    @Expose
    private String priceConditionsPosition;
    @SerializedName("price_conditions_id")
    @Expose
    private String priceConditionsId;
    @SerializedName("sku")
    @Expose
    private String sku;

    public String getPriceNotAvailable() {
        return priceNotAvailable;
    }

    public void setPriceNotAvailable(String priceNotAvailable) {
        this.priceNotAvailable = priceNotAvailable;
    }

    public String getOriginalPriceCurrency() {
        return originalPriceCurrency;
    }

    public void setOriginalPriceCurrency(String originalPriceCurrency) {
        this.originalPriceCurrency = originalPriceCurrency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getPriceConditionsPosition() {
        return priceConditionsPosition;
    }

    public void setPriceConditionsPosition(String priceConditionsPosition) {
        this.priceConditionsPosition = priceConditionsPosition;
    }

    public String getPriceConditionsId() {
        return priceConditionsId;
    }

    public void setPriceConditionsId(String priceConditionsId) {
        this.priceConditionsId = priceConditionsId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

}
