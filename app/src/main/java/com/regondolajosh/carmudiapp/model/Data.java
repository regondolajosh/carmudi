package com.regondolajosh.carmudiapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable, Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };
    @SerializedName("fk_country")
    @Expose
    private String fkCountry;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("attribute_set_name")
    @Expose
    private String attributeSetName;
    @SerializedName("attribute_set_name_local")
    @Expose
    private String attributeSetNameLocal;
    @SerializedName("approved")
    @Expose
    private String approved;
    @SerializedName("status_supplier_config")
    @Expose
    private String statusSupplierConfig;
    @SerializedName("activated_at")
    @Expose
    private String activatedAt;
    @SerializedName("listing_start")
    @Expose
    private String listingStart;
    @SerializedName("listing_end")
    @Expose
    private String listingEnd;
    @SerializedName("fk_vertical")
    @Expose
    private String fkVertical;
    @SerializedName("fk_catalog_brand")
    @Expose
    private String fkCatalogBrand;
    @SerializedName("fk_catalog_brand_model")
    @Expose
    private String fkCatalogBrandModel;
    @SerializedName("brand_model_edition")
    @Expose
    private String brandModelEdition;
    @SerializedName("listing_type")
    @Expose
    private String listingType;
    @SerializedName("listing_country")
    @Expose
    private String listingCountry;
    @SerializedName("listing_area")
    @Expose
    private String listingArea;
    @SerializedName("condition")
    @Expose
    private String condition;
    @SerializedName("condition_position")
    @Expose
    private String conditionPosition;
    @SerializedName("condition_id")
    @Expose
    private String conditionId;
    @SerializedName("color_family_position")
    @Expose
    private String colorFamilyPosition;
    @SerializedName("color_family_id")
    @Expose
    private String colorFamilyId;
    @SerializedName("doors_position")
    @Expose
    private String doorsPosition;
    @SerializedName("doors_id")
    @Expose
    private String doorsId;
    @SerializedName("power")
    @Expose
    private String power;
    @SerializedName("drive_type_position")
    @Expose
    private String driveTypePosition;
    @SerializedName("drive_type_id")
    @Expose
    private int driveTypeId;
    @SerializedName("interior")
    @Expose
    private String interior;
    @SerializedName("exterior")
    @Expose
    private String exterior;
    @SerializedName("equipment")
    @Expose
    private String equipment;
    @SerializedName("warranty_type_position")
    @Expose
    private String warrantyTypePosition;
    @SerializedName("warranty_type_id")
    @Expose
    private String warrantyTypeId;
    @SerializedName("warranty_years_position")
    @Expose
    private String warrantyYearsPosition;
    @SerializedName("warranty_years_id")
    @Expose
    private String warrantyYearsId;
    @SerializedName("price_conditions_position")
    @Expose
    private String priceConditionsPosition;
    @SerializedName("price_conditions_id")
    @Expose
    private String priceConditionsId;
    @SerializedName("premium_listing")
    @Expose
    private String premiumListing;
    @SerializedName("alternate_sku")
    @Expose
    private String alternateSku;
    @SerializedName("original_name")
    @Expose
    private String originalName;
    @SerializedName("root_category")
    @Expose
    private String rootCategory;
    @SerializedName("agency_logo")
    @Expose
    private String agencyLogo;
    @SerializedName("new-product")
    @Expose
    private boolean newProduct;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("location_latitude")
    @Expose
    private String locationLatitude;
    @SerializedName("location_longitude")
    @Expose
    private String locationLongitude;
    @SerializedName("google_formatted_address")
    @Expose
    private String googleFormattedAddress;
    @SerializedName("google_place_id")
    @Expose
    private String googlePlaceId;
    @SerializedName("fk_customer_address")
    @Expose
    private String fkCustomerAddress;
    @SerializedName("listing_region")
    @Expose
    private String listingRegion;
    @SerializedName("listing_city")
    @Expose
    private String listingCity;
    @SerializedName("agency_address")
    @Expose
    private String agencyAddress;
    @SerializedName("agency_city")
    @Expose
    private String agencyCity;
    @SerializedName("fk_country_region")
    @Expose
    private String fkCountryRegion;
    @SerializedName("fk_country_region_city")
    @Expose
    private String fkCountryRegionCity;
    @SerializedName("fk_country_region_city_area")
    @Expose
    private String fkCountryRegionCityArea;
    @SerializedName("show_listing_address")
    @Expose
    private String showListingAddress;
    @SerializedName("item_contact_name")
    @Expose
    private String itemContactName;
    @SerializedName("item_contact_email")
    @Expose
    private String itemContactEmail;
    @SerializedName("item_contact_mobile")
    @Expose
    private String itemContactMobile;
    @SerializedName("item_contact_homephone")
    @Expose
    private String itemContactHomephone;
    @SerializedName("agency_name")
    @Expose
    private String agencyName;
    @SerializedName("product_owner_url_key")
    @Expose
    private String productOwnerUrlKey;
    @SerializedName("product_owner")
    @Expose
    private String productOwner;
    @SerializedName("fk_customer")
    @Expose
    private String fkCustomer;
    @SerializedName("is_agent")
    @Expose
    private String isAgent;
    @SerializedName("seller_is_trusted")
    @Expose
    private String sellerIsTrusted;
    @SerializedName("show_officephone")
    @Expose
    private String showOfficephone;
    @SerializedName("show_homephone")
    @Expose
    private String showHomephone;
    @SerializedName("show_mobile")
    @Expose
    private String showMobile;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("id_catalog_config")
    @Expose
    private String idCatalogConfig;
    @SerializedName("attribute_set_id")
    @Expose
    private String attributeSetId;
    @SerializedName("original_price_currency")
    @Expose
    private String originalPriceCurrency;
    @SerializedName("is_certified")
    @Expose
    private int isCertified;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("urlkey_details")
    @Expose
    private String urlkeyDetails;
    @SerializedName("price_not_available")
    @Expose
    private String priceNotAvailable;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("original_price")
    @Expose
    private String originalPrice;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("brand_model")
    @Expose
    private String brandModel;
    @SerializedName("top_position")
    @Expose
    private String topPosition;
    @SerializedName("mileage_not_available")
    @Expose
    private String mileageNotAvailable;
    @SerializedName("mileage")
    @Expose
    private String mileage;
    @SerializedName("config_id")
    @Expose
    private String configId;
    @SerializedName("rejected_comment")
    @Expose
    private String rejectedComment;
    @SerializedName("transmission")
    @Expose
    private String transmission;
    @SerializedName("transmission_position")
    @Expose
    private String transmissionPosition;
    @SerializedName("transmission_id")
    @Expose
    private String transmissionId;
    @SerializedName("fuel")
    @Expose
    private String fuel;
    @SerializedName("fuel_position")
    @Expose
    private String fuelPosition;
    @SerializedName("fuel_id")
    @Expose
    private String fuelId;
    @SerializedName("simples")
    @Expose
    private Simples simples;
    @SerializedName("attributes")
    @Expose
    private Attributes attributes;

    protected Data(Parcel in) {
        fkCountry = in.readString();
        status = in.readString();
        attributeSetName = in.readString();
        attributeSetNameLocal = in.readString();
        approved = in.readString();
        statusSupplierConfig = in.readString();
        activatedAt = in.readString();
        listingStart = in.readString();
        listingEnd = in.readString();
        fkVertical = in.readString();
        fkCatalogBrand = in.readString();
        fkCatalogBrandModel = in.readString();
        brandModelEdition = in.readString();
        listingType = in.readString();
        listingCountry = in.readString();
        listingArea = in.readString();
        condition = in.readString();
        conditionPosition = in.readString();
        conditionId = in.readString();
        colorFamilyPosition = in.readString();
        colorFamilyId = in.readString();
        doorsPosition = in.readString();
        doorsId = in.readString();
        power = in.readString();
        driveTypePosition = in.readString();
        driveTypeId = in.readInt();
        interior = in.readString();
        exterior = in.readString();
        equipment = in.readString();
        warrantyTypePosition = in.readString();
        warrantyTypeId = in.readString();
        warrantyYearsPosition = in.readString();
        warrantyYearsId = in.readString();
        priceConditionsPosition = in.readString();
        priceConditionsId = in.readString();
        premiumListing = in.readString();
        alternateSku = in.readString();
        originalName = in.readString();
        rootCategory = in.readString();
        agencyLogo = in.readString();
        newProduct = in.readByte() != 0x00;
        url = in.readString();
        id = in.readString();
        locationLatitude = in.readString();
        locationLongitude = in.readString();
        googleFormattedAddress = in.readString();
        googlePlaceId = in.readString();
        fkCustomerAddress = in.readString();
        listingRegion = in.readString();
        listingCity = in.readString();
        agencyAddress = in.readString();
        agencyCity = in.readString();
        fkCountryRegion = in.readString();
        fkCountryRegionCity = in.readString();
        fkCountryRegionCityArea = in.readString();
        showListingAddress = in.readString();
        itemContactName = in.readString();
        itemContactEmail = in.readString();
        itemContactMobile = in.readString();
        itemContactHomephone = in.readString();
        agencyName = in.readString();
        productOwnerUrlKey = in.readString();
        productOwner = in.readString();
        fkCustomer = in.readString();
        isAgent = in.readString();
        sellerIsTrusted = in.readString();
        showOfficephone = in.readString();
        showHomephone = in.readString();
        showMobile = in.readString();
        sku = in.readString();
        idCatalogConfig = in.readString();
        attributeSetId = in.readString();
        originalPriceCurrency = in.readString();
        isCertified = in.readInt();
        name = in.readString();
        description = in.readString();
        urlkeyDetails = in.readString();
        priceNotAvailable = in.readString();
        price = in.readString();
        originalPrice = in.readString();
        brand = in.readString();
        brandModel = in.readString();
        topPosition = in.readString();
        mileageNotAvailable = in.readString();
        mileage = in.readString();
        configId = in.readString();
        rejectedComment = in.readString();
        transmission = in.readString();
        transmissionPosition = in.readString();
        transmissionId = in.readString();
        fuel = in.readString();
        fuelPosition = in.readString();
        fuelId = in.readString();
        simples = (Simples) in.readValue(Simples.class.getClassLoader());
        attributes = (Attributes) in.readValue(Attributes.class.getClassLoader());
    }

    public String getFkCountry() {
        return fkCountry;
    }

    public void setFkCountry(String fkCountry) {
        this.fkCountry = fkCountry;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAttributeSetName() {
        return attributeSetName;
    }

    public void setAttributeSetName(String attributeSetName) {
        this.attributeSetName = attributeSetName;
    }

    public String getAttributeSetNameLocal() {
        return attributeSetNameLocal;
    }

    public void setAttributeSetNameLocal(String attributeSetNameLocal) {
        this.attributeSetNameLocal = attributeSetNameLocal;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public String getStatusSupplierConfig() {
        return statusSupplierConfig;
    }

    public void setStatusSupplierConfig(String statusSupplierConfig) {
        this.statusSupplierConfig = statusSupplierConfig;
    }

    public String getActivatedAt() {
        return activatedAt;
    }

    public void setActivatedAt(String activatedAt) {
        this.activatedAt = activatedAt;
    }

    public String getListingStart() {
        return listingStart;
    }

    public void setListingStart(String listingStart) {
        this.listingStart = listingStart;
    }

    public String getListingEnd() {
        return listingEnd;
    }

    public void setListingEnd(String listingEnd) {
        this.listingEnd = listingEnd;
    }

    public String getFkVertical() {
        return fkVertical;
    }

    public void setFkVertical(String fkVertical) {
        this.fkVertical = fkVertical;
    }

    public String getFkCatalogBrand() {
        return fkCatalogBrand;
    }

    public void setFkCatalogBrand(String fkCatalogBrand) {
        this.fkCatalogBrand = fkCatalogBrand;
    }

    public String getFkCatalogBrandModel() {
        return fkCatalogBrandModel;
    }

    public void setFkCatalogBrandModel(String fkCatalogBrandModel) {
        this.fkCatalogBrandModel = fkCatalogBrandModel;
    }

    public String getBrandModelEdition() {
        return brandModelEdition;
    }

    public void setBrandModelEdition(String brandModelEdition) {
        this.brandModelEdition = brandModelEdition;
    }

    public String getListingType() {
        return listingType;
    }

    public void setListingType(String listingType) {
        this.listingType = listingType;
    }

    public String getListingCountry() {
        return listingCountry;
    }

    public void setListingCountry(String listingCountry) {
        this.listingCountry = listingCountry;
    }

    public String getListingArea() {
        return listingArea;
    }

    public void setListingArea(String listingArea) {
        this.listingArea = listingArea;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getConditionPosition() {
        return conditionPosition;
    }

    public void setConditionPosition(String conditionPosition) {
        this.conditionPosition = conditionPosition;
    }

    public String getConditionId() {
        return conditionId;
    }

    public void setConditionId(String conditionId) {
        this.conditionId = conditionId;
    }

    public String getColorFamilyPosition() {
        return colorFamilyPosition;
    }

    public void setColorFamilyPosition(String colorFamilyPosition) {
        this.colorFamilyPosition = colorFamilyPosition;
    }

    public String getColorFamilyId() {
        return colorFamilyId;
    }

    public void setColorFamilyId(String colorFamilyId) {
        this.colorFamilyId = colorFamilyId;
    }

    public String getDoorsPosition() {
        return doorsPosition;
    }

    public void setDoorsPosition(String doorsPosition) {
        this.doorsPosition = doorsPosition;
    }

    public String getDoorsId() {
        return doorsId;
    }

    public void setDoorsId(String doorsId) {
        this.doorsId = doorsId;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getDriveTypePosition() {
        return driveTypePosition;
    }

    public void setDriveTypePosition(String driveTypePosition) {
        this.driveTypePosition = driveTypePosition;
    }

    public int getDriveTypeId() {
        return driveTypeId;
    }

    public void setDriveTypeId(int driveTypeId) {
        this.driveTypeId = driveTypeId;
    }

    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    public String getExterior() {
        return exterior;
    }

    public void setExterior(String exterior) {
        this.exterior = exterior;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getWarrantyTypePosition() {
        return warrantyTypePosition;
    }

    public void setWarrantyTypePosition(String warrantyTypePosition) {
        this.warrantyTypePosition = warrantyTypePosition;
    }

    public String getWarrantyTypeId() {
        return warrantyTypeId;
    }

    public void setWarrantyTypeId(String warrantyTypeId) {
        this.warrantyTypeId = warrantyTypeId;
    }

    public String getWarrantyYearsPosition() {
        return warrantyYearsPosition;
    }

    public void setWarrantyYearsPosition(String warrantyYearsPosition) {
        this.warrantyYearsPosition = warrantyYearsPosition;
    }

    public String getWarrantyYearsId() {
        return warrantyYearsId;
    }

    public void setWarrantyYearsId(String warrantyYearsId) {
        this.warrantyYearsId = warrantyYearsId;
    }

    public String getPriceConditionsPosition() {
        return priceConditionsPosition;
    }

    public void setPriceConditionsPosition(String priceConditionsPosition) {
        this.priceConditionsPosition = priceConditionsPosition;
    }

    public String getPriceConditionsId() {
        return priceConditionsId;
    }

    public void setPriceConditionsId(String priceConditionsId) {
        this.priceConditionsId = priceConditionsId;
    }

    public String getPremiumListing() {
        return premiumListing;
    }

    public void setPremiumListing(String premiumListing) {
        this.premiumListing = premiumListing;
    }

    public String getAlternateSku() {
        return alternateSku;
    }

    public void setAlternateSku(String alternateSku) {
        this.alternateSku = alternateSku;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getRootCategory() {
        return rootCategory;
    }

    public void setRootCategory(String rootCategory) {
        this.rootCategory = rootCategory;
    }

    public String getAgencyLogo() {
        return agencyLogo;
    }

    public void setAgencyLogo(String agencyLogo) {
        this.agencyLogo = agencyLogo;
    }

    public boolean isNewProduct() {
        return newProduct;
    }

    public void setNewProduct(boolean newProduct) {
        this.newProduct = newProduct;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocationLatitude() {
        return locationLatitude;
    }

    public void setLocationLatitude(String locationLatitude) {
        this.locationLatitude = locationLatitude;
    }

    public String getLocationLongitude() {
        return locationLongitude;
    }

    public void setLocationLongitude(String locationLongitude) {
        this.locationLongitude = locationLongitude;
    }

    public String getGoogleFormattedAddress() {
        return googleFormattedAddress;
    }

    public void setGoogleFormattedAddress(String googleFormattedAddress) {
        this.googleFormattedAddress = googleFormattedAddress;
    }

    public String getGooglePlaceId() {
        return googlePlaceId;
    }

    public void setGooglePlaceId(String googlePlaceId) {
        this.googlePlaceId = googlePlaceId;
    }

    public String getFkCustomerAddress() {
        return fkCustomerAddress;
    }

    public void setFkCustomerAddress(String fkCustomerAddress) {
        this.fkCustomerAddress = fkCustomerAddress;
    }

    public String getListingRegion() {
        return listingRegion;
    }

    public void setListingRegion(String listingRegion) {
        this.listingRegion = listingRegion;
    }

    public String getListingCity() {
        return listingCity;
    }

    public void setListingCity(String listingCity) {
        this.listingCity = listingCity;
    }

    public String getAgencyAddress() {
        return agencyAddress;
    }

    public void setAgencyAddress(String agencyAddress) {
        this.agencyAddress = agencyAddress;
    }

    public String getAgencyCity() {
        return agencyCity;
    }

    public void setAgencyCity(String agencyCity) {
        this.agencyCity = agencyCity;
    }

    public String getFkCountryRegion() {
        return fkCountryRegion;
    }

    public void setFkCountryRegion(String fkCountryRegion) {
        this.fkCountryRegion = fkCountryRegion;
    }

    public String getFkCountryRegionCity() {
        return fkCountryRegionCity;
    }

    public void setFkCountryRegionCity(String fkCountryRegionCity) {
        this.fkCountryRegionCity = fkCountryRegionCity;
    }

    public String getFkCountryRegionCityArea() {
        return fkCountryRegionCityArea;
    }

    public void setFkCountryRegionCityArea(String fkCountryRegionCityArea) {
        this.fkCountryRegionCityArea = fkCountryRegionCityArea;
    }

    public String getShowListingAddress() {
        return showListingAddress;
    }

    public void setShowListingAddress(String showListingAddress) {
        this.showListingAddress = showListingAddress;
    }

    public String getItemContactName() {
        return itemContactName;
    }

    public void setItemContactName(String itemContactName) {
        this.itemContactName = itemContactName;
    }

    public String getItemContactEmail() {
        return itemContactEmail;
    }

    public void setItemContactEmail(String itemContactEmail) {
        this.itemContactEmail = itemContactEmail;
    }

    public String getItemContactMobile() {
        return itemContactMobile;
    }

    public void setItemContactMobile(String itemContactMobile) {
        this.itemContactMobile = itemContactMobile;
    }

    public String getItemContactHomephone() {
        return itemContactHomephone;
    }

    public void setItemContactHomephone(String itemContactHomephone) {
        this.itemContactHomephone = itemContactHomephone;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getProductOwnerUrlKey() {
        return productOwnerUrlKey;
    }

    public void setProductOwnerUrlKey(String productOwnerUrlKey) {
        this.productOwnerUrlKey = productOwnerUrlKey;
    }

    public String getProductOwner() {
        return productOwner;
    }

    public void setProductOwner(String productOwner) {
        this.productOwner = productOwner;
    }

    public String getFkCustomer() {
        return fkCustomer;
    }

    public void setFkCustomer(String fkCustomer) {
        this.fkCustomer = fkCustomer;
    }

    public String getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(String isAgent) {
        this.isAgent = isAgent;
    }

    public String getSellerIsTrusted() {
        return sellerIsTrusted;
    }

    public void setSellerIsTrusted(String sellerIsTrusted) {
        this.sellerIsTrusted = sellerIsTrusted;
    }

    public String getShowOfficephone() {
        return showOfficephone;
    }

    public void setShowOfficephone(String showOfficephone) {
        this.showOfficephone = showOfficephone;
    }

    public String getShowHomephone() {
        return showHomephone;
    }

    public void setShowHomephone(String showHomephone) {
        this.showHomephone = showHomephone;
    }

    public String getShowMobile() {
        return showMobile;
    }

    public void setShowMobile(String showMobile) {
        this.showMobile = showMobile;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getIdCatalogConfig() {
        return idCatalogConfig;
    }

    public void setIdCatalogConfig(String idCatalogConfig) {
        this.idCatalogConfig = idCatalogConfig;
    }

    public String getAttributeSetId() {
        return attributeSetId;
    }

    public void setAttributeSetId(String attributeSetId) {
        this.attributeSetId = attributeSetId;
    }

    public String getOriginalPriceCurrency() {
        return originalPriceCurrency;
    }

    public void setOriginalPriceCurrency(String originalPriceCurrency) {
        this.originalPriceCurrency = originalPriceCurrency;
    }

    public int getIsCertified() {
        return isCertified;
    }

    public void setIsCertified(int isCertified) {
        this.isCertified = isCertified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlkeyDetails() {
        return urlkeyDetails;
    }

    public void setUrlkeyDetails(String urlkeyDetails) {
        this.urlkeyDetails = urlkeyDetails;
    }

    public String getPriceNotAvailable() {
        return priceNotAvailable;
    }

    public void setPriceNotAvailable(String priceNotAvailable) {
        this.priceNotAvailable = priceNotAvailable;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrandModel() {
        return brandModel;
    }

    public void setBrandModel(String brandModel) {
        this.brandModel = brandModel;
    }

    public String getTopPosition() {
        return topPosition;
    }

    public void setTopPosition(String topPosition) {
        this.topPosition = topPosition;
    }

    public String getMileageNotAvailable() {
        return mileageNotAvailable;
    }

    public void setMileageNotAvailable(String mileageNotAvailable) {
        this.mileageNotAvailable = mileageNotAvailable;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getRejectedComment() {
        return rejectedComment;
    }

    public void setRejectedComment(String rejectedComment) {
        this.rejectedComment = rejectedComment;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getTransmissionPosition() {
        return transmissionPosition;
    }

    public void setTransmissionPosition(String transmissionPosition) {
        this.transmissionPosition = transmissionPosition;
    }

    public String getTransmissionId() {
        return transmissionId;
    }

    public void setTransmissionId(String transmissionId) {
        this.transmissionId = transmissionId;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getFuelPosition() {
        return fuelPosition;
    }

    public void setFuelPosition(String fuelPosition) {
        this.fuelPosition = fuelPosition;
    }

    public String getFuelId() {
        return fuelId;
    }

    public void setFuelId(String fuelId) {
        this.fuelId = fuelId;
    }

    public Simples getSimples() {
        return simples;
    }

    public void setSimples(Simples simples) {
        this.simples = simples;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fkCountry);
        dest.writeString(status);
        dest.writeString(attributeSetName);
        dest.writeString(attributeSetNameLocal);
        dest.writeString(approved);
        dest.writeString(statusSupplierConfig);
        dest.writeString(activatedAt);
        dest.writeString(listingStart);
        dest.writeString(listingEnd);
        dest.writeString(fkVertical);
        dest.writeString(fkCatalogBrand);
        dest.writeString(fkCatalogBrandModel);
        dest.writeString(brandModelEdition);
        dest.writeString(listingType);
        dest.writeString(listingCountry);
        dest.writeString(listingArea);
        dest.writeString(condition);
        dest.writeString(conditionPosition);
        dest.writeString(conditionId);
        dest.writeString(colorFamilyPosition);
        dest.writeString(colorFamilyId);
        dest.writeString(doorsPosition);
        dest.writeString(doorsId);
        dest.writeString(power);
        dest.writeString(driveTypePosition);
        dest.writeInt(driveTypeId);
        dest.writeString(interior);
        dest.writeString(exterior);
        dest.writeString(equipment);
        dest.writeString(warrantyTypePosition);
        dest.writeString(warrantyTypeId);
        dest.writeString(warrantyYearsPosition);
        dest.writeString(warrantyYearsId);
        dest.writeString(priceConditionsPosition);
        dest.writeString(priceConditionsId);
        dest.writeString(premiumListing);
        dest.writeString(alternateSku);
        dest.writeString(originalName);
        dest.writeString(rootCategory);
        dest.writeString(agencyLogo);
        dest.writeByte((byte) (newProduct ? 0x01 : 0x00));
        dest.writeString(url);
        dest.writeString(id);
        dest.writeString(locationLatitude);
        dest.writeString(locationLongitude);
        dest.writeString(googleFormattedAddress);
        dest.writeString(googlePlaceId);
        dest.writeString(fkCustomerAddress);
        dest.writeString(listingRegion);
        dest.writeString(listingCity);
        dest.writeString(agencyAddress);
        dest.writeString(agencyCity);
        dest.writeString(fkCountryRegion);
        dest.writeString(fkCountryRegionCity);
        dest.writeString(fkCountryRegionCityArea);
        dest.writeString(showListingAddress);
        dest.writeString(itemContactName);
        dest.writeString(itemContactEmail);
        dest.writeString(itemContactMobile);
        dest.writeString(itemContactHomephone);
        dest.writeString(agencyName);
        dest.writeString(productOwnerUrlKey);
        dest.writeString(productOwner);
        dest.writeString(fkCustomer);
        dest.writeString(isAgent);
        dest.writeString(sellerIsTrusted);
        dest.writeString(showOfficephone);
        dest.writeString(showHomephone);
        dest.writeString(showMobile);
        dest.writeString(sku);
        dest.writeString(idCatalogConfig);
        dest.writeString(attributeSetId);
        dest.writeString(originalPriceCurrency);
        dest.writeInt(isCertified);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(urlkeyDetails);
        dest.writeString(priceNotAvailable);
        dest.writeString(price);
        dest.writeString(originalPrice);
        dest.writeString(brand);
        dest.writeString(brandModel);
        dest.writeString(topPosition);
        dest.writeString(mileageNotAvailable);
        dest.writeString(mileage);
        dest.writeString(configId);
        dest.writeString(rejectedComment);
        dest.writeString(transmission);
        dest.writeString(transmissionPosition);
        dest.writeString(transmissionId);
        dest.writeString(fuel);
        dest.writeString(fuelPosition);
        dest.writeString(fuelId);
        dest.writeValue(simples);
        dest.writeValue(attributes);
    }
}