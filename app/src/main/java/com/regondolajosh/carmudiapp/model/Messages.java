package com.regondolajosh.carmudiapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Messages implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Messages> CREATOR = new Parcelable.Creator<Messages>() {
        @Override
        public Messages createFromParcel(Parcel in) {
            return new Messages(in);
        }

        @Override
        public Messages[] newArray(int size) {
            return new Messages[size];
        }
    };
    @SerializedName("success")
    @Expose
    private List<String> success = null;

    protected Messages(Parcel in) {
        if (in.readByte() == 0x01) {
            success = new ArrayList<String>();
            in.readList(success, String.class.getClassLoader());
        } else {
            success = null;
        }
    }

    public List<String> getSuccess() {
        return success;
    }

    public void setSuccess(List<String> success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (success == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(success);
        }
    }
}