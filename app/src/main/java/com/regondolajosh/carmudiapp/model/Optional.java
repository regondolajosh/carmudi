package com.regondolajosh.carmudiapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Optional implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Optional> CREATOR = new Parcelable.Creator<Optional>() {
        @Override
        public Optional createFromParcel(Parcel in) {
            return new Optional(in);
        }

        @Override
        public Optional[] newArray(int size) {
            return new Optional[size];
        }
    };
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("label_en")
    @Expose
    private String labelEn;
    @SerializedName("options")
    @Expose
    private List<String> options = null;

    protected Optional(Parcel in) {
        name = in.readString();
        label = in.readString();
        labelEn = in.readString();
        if (in.readByte() == 0x01) {
            options = new ArrayList<String>();
            in.readList(options, String.class.getClassLoader());
        } else {
            options = null;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabelEn() {
        return labelEn;
    }

    public void setLabelEn(String labelEn) {
        this.labelEn = labelEn;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(label);
        dest.writeString(labelEn);
        if (options == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(options);
        }
    }
}