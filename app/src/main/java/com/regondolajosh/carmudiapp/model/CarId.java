package com.regondolajosh.carmudiapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarId implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CarId> CREATOR = new Parcelable.Creator<CarId>() {
        @Override
        public CarId createFromParcel(Parcel in) {
            return new CarId(in);
        }

        @Override
        public CarId[] newArray(int size) {
            return new CarId[size];
        }
    };
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("priceAttributes")
    @Expose
    private PriceAttributes priceAttributes;

    protected CarId(Parcel in) {
        meta = (Meta) in.readValue(Meta.class.getClassLoader());
        priceAttributes = (PriceAttributes) in.readValue(PriceAttributes.class.getClassLoader());
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public PriceAttributes getPriceAttributes() {
        return priceAttributes;
    }

    public void setPriceAttributes(PriceAttributes priceAttributes) {
        this.priceAttributes = priceAttributes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(meta);
        dest.writeValue(priceAttributes);
    }
}