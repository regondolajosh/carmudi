package com.regondolajosh.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Session {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("expire")
    @Expose
    private String expire;
    @SerializedName("YII_CSRF_TOKEN")
    @Expose
    private String yIICSRFTOKEN;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getYIICSRFTOKEN() {
        return yIICSRFTOKEN;
    }

    public void setYIICSRFTOKEN(String yIICSRFTOKEN) {
        this.yIICSRFTOKEN = yIICSRFTOKEN;
    }

}
