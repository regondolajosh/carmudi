package com.regondolajosh.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAdsListResponseModel {

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("messages")
    @Expose
    private Messages messages;
    @SerializedName("session")
    @Expose
    private Session session;
    @SerializedName("metadata")
    @Expose
    private Metadata metadata;
    public GetAdsListResponseModel() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Messages getMessages() {
        return messages;
    }

    public void setMessages(Messages messages) {
        this.messages = messages;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

}
