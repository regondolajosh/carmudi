package com.regondolajosh.carmudiapp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.regondolajosh.carmudiapp.R;
import com.regondolajosh.carmudiapp.constant.PrefKey;
import com.regondolajosh.carmudiapp.model.Data;
import com.regondolajosh.carmudiapp.model.Result;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by henry on 2/8/17.
 */

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ResultViewHolder> {

    private Context context;
    private List<Result> resultList;
    private LayoutInflater layoutInflater;

    public ResultAdapter(Context context, List<Result> resultList) {
        this.context = context;
        this.resultList = resultList;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_ad, parent, false);
        ResultViewHolder vehicleViewHolder = new ResultViewHolder(view);
        return vehicleViewHolder;
    }

    @Override
    public void onBindViewHolder(ResultViewHolder holder, int position) {
        final Result result = resultList.get(position);
        final Data data = result.getData();
        if (result != null && holder != null) {
            if (result.getImages().get(0) != null) {
                Glide.with(context)
                        .load(result.getImages().get(0).getUrl())
                        .into(holder.ivImage);
            }
            holder.tvName.setText(data.getName());
            holder.tvPrice.setText(data.getPrice());
            holder.tvCondition.setText(data.getCondition());
            holder.tvFuel.setText(data.getFuel());
            holder.tvTransmission.setText(data.getTransmission());
            holder.tvMileage.setText(data.getMileage());
            holder.tvBrand.setText(data.getBrand());

        }

    }

    public List<Result> getResultList() {
        return resultList;
    }

    public void setResultList(List<Result> resultList) {
        this.resultList = resultList;
    }

    @Override
    public int getItemCount() {
        return resultList == null ? 0 : resultList.size();
    }

    public class ResultViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        @BindView(R.id.rlAd)
        public RelativeLayout rlVehicle;
        @Nullable
        @BindView(R.id.ivImage)
        public ImageView ivImage;
        @Nullable
        @BindView(R.id.tvName)
        public TextView tvName;
        @Nullable
        @BindView(R.id.tvPrice)
        public TextView tvPrice;
        @Nullable
        @BindView(R.id.tvCondition)
        public TextView tvCondition;
        @Nullable
        @BindView(R.id.tvTransmission)
        public TextView tvTransmission;
        @Nullable
        @BindView(R.id.tvFuel)
        public TextView tvFuel;
        @Nullable
        @BindView(R.id.tvMileage)
        public TextView tvMileage;
        @Nullable
        @BindView(R.id.tvBrand)
        public TextView tvBrand;

        public ResultViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            rlVehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ResultDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(PrefKey.RESULT, resultList.get(getAdapterPosition()));
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });

        }


    }
}
