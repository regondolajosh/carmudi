package com.regondolajosh.carmudiapp.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.regondolajosh.carmudiapp.R;
import com.regondolajosh.carmudiapp.constant.PrefKey;
import com.regondolajosh.carmudiapp.model.Result;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rego on 12/1/18.
 */

public class ResultDetailActivity extends AppCompatActivity {

    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvBrand)
    TextView tvBrand;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.tvTransmission)
    TextView tvTransmission;
    @BindView(R.id.tvFuel)
    TextView tvFuel;
    @BindView(R.id.tvMileage)
    TextView tvMileage;
    @BindView(R.id.tvCondition)
    TextView tvCondition;

    private Result result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_detail);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            result = getIntent().getExtras().getParcelable(PrefKey.RESULT);
        }
        if (result != null) {
            if (result.getImages().get(0) != null) {
                Glide.with(this)
                        .load(result.getImages().get(0).getUrl())
                        .into(ivImage);
            }

            tvName.setText(result.getData().getName());
            tvBrand.setText(result.getData().getBrand());
            tvTransmission.setText(result.getData().getTransmission());
            tvFuel.setText(result.getData().getFuel());
            tvMileage.setText(result.getData().getMileage());
            tvCondition.setText(result.getData().getCondition());
            tvPrice.setText(result.getData().getPrice());
        }

    }
}
