package com.regondolajosh.carmudiapp.view;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.regondolajosh.carmudiapp.R;
import com.regondolajosh.carmudiapp.contract.MainContract;
import com.regondolajosh.carmudiapp.model.GetAdsListResponseModel;
import com.regondolajosh.carmudiapp.model.Result;
import com.regondolajosh.carmudiapp.presenter.MainActivityPresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainContract.View, SwipeRefreshLayout.OnRefreshListener,
        OnMoreListener, Spinner.OnItemSelectedListener {

    private static int MAX_ITEMS = 10;
    @BindView(R.id.rvAds)
    SuperRecyclerView rvResults;
    @BindView(R.id.srlAds)
    SwipeRefreshLayout srlAds;
    private MainContract.Presenter presenter;
    private ArrayList<Result> results;
    private ResultAdapter resultAdapter;
    private Spinner spinner;
    private String sortKey;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        results = new ArrayList<>();
        rvResults.setLayoutManager(new LinearLayoutManager(this));
        resultAdapter = new ResultAdapter(this, results);
        rvResults.setAdapter(resultAdapter);
        rvResults.setupMoreListener(this, 1);
        srlAds.setOnRefreshListener(this);


        sortKey = "";

        if (presenter == null) {
            presenter = new MainActivityPresenter();
            presenter.attach(this);
            presenter.subscribe(page, MAX_ITEMS, sortKey);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        spinner = (Spinner) MenuItemCompat.getActionView(item);
        spinner.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.sort_key, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        return true;
    }

    @Override
    public void onFetchDataStarted() {
        srlAds.setRefreshing(true);

    }

    @Override
    public void onFetchDataCompleted() {
        srlAds.setRefreshing(false);

    }

    @Override
    public void onFetchDataSuccess(GetAdsListResponseModel responseModel) {
        onFetchDataCompleted();
        if (results != null) {
            if (page == 1) {
                results.clear();
            }
            results.addAll(responseModel.getMetadata().getResults());
            resultAdapter.setResultList(results);
            resultAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onDestroy() {
        if (presenter != null) {
            presenter.unSubscribe();
            presenter.onDestroy();
        }
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onFetchDataError(String message) {
        srlAds.setRefreshing(false);
        rvResults.setRefreshing(false);
        rvResults.getMoreProgressView().setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    private void refreshList() {
        page = 1;
        sortKey = spinner.getSelectedItem().toString();
        presenter.subscribe(page, MAX_ITEMS, sortKey);
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        if (overallItemsCount < MAX_ITEMS) {
            page = 1;
        } else {
            page++;
        }
        sortKey = spinner.getSelectedItem().toString();
        presenter.subscribe(page, MAX_ITEMS, sortKey);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        refreshList();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
