package com.regondolajosh.carmudiapp.constant;

/**
 * Created by Rego on 12/1/18.
 */

public class PrefKey {
    public static String LIST = "results_list";
    public static String RESULT = "result";
}
