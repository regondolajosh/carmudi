package com.regondolajosh.carmudiapp.network;

import com.regondolajosh.carmudiapp.model.GetAdsListResponseModel;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Rego on 12/1/18.
 */

public interface GetAdsListService {

    @GET("cars")
    Observable<GetAdsListResponseModel> getAdsList(@Query("page") int page, @Query("maxitems") int maxItems, @Query("sort") String sortKey);
}
