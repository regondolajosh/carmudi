package com.regondolajosh.carmudiapp.manager;


import com.regondolajosh.carmudiapp.BuildConfig;
import com.regondolajosh.carmudiapp.network.GetAdsListService;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


/**
 * Created by olivier.goutay on 4/28/17.
 */

public final class HttpManager {


    private static volatile HttpManager httpManager;

    private GetAdsListService getAdsListService;

    private HttpManager() {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient.build())
                .build();
        getAdsListService = retrofit.create(GetAdsListService.class);
    }


    public static HttpManager getInstance() {
        synchronized (HttpManager.class) {
            if (httpManager == null) {
                httpManager = new HttpManager();
            }
        }
        return httpManager;
    }

    public GetAdsListService getGetAdsListService() {
        return getAdsListService;
    }

    public void setGetAdsListService(GetAdsListService getAdsListService) {
        this.getAdsListService = getAdsListService;
    }


}
