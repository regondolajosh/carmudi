package com.regondolajosh.carmudiapp.contract;

import com.regondolajosh.carmudiapp.model.GetAdsListResponseModel;

/**
 * Created by Rego on 11/1/18.
 */

public interface MainContract {

    interface View {

        void onFetchDataStarted();

        void onFetchDataCompleted();

        void onFetchDataSuccess(GetAdsListResponseModel responseModel);

        void onFetchDataError(String message);
    }

    interface Presenter extends BasePresenter<MainContract.View> {

        void loadData(int page, int maxItems, String sortKey);

        void subscribe(int page, int maxItems, String sortKey);

        void unSubscribe();

        void onDestroy();

    }


}
