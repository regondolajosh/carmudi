package com.regondolajosh.carmudiapp.presenter;

import com.regondolajosh.carmudiapp.contract.MainContract;
import com.regondolajosh.carmudiapp.manager.HttpManager;
import com.regondolajosh.carmudiapp.model.GetAdsListResponseModel;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Rego on 11/1/18.
 */

public class MainActivityPresenter implements MainContract.Presenter {

    public MainContract.View view;


    @Override
    public void attach(MainContract.View view) {
        this.view = view;
    }


    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void loadData(int page, int maxItems, String sortKey) {
        if (view != null) {
            view.onFetchDataStarted();
            Observable<GetAdsListResponseModel> results = HttpManager.getInstance().getGetAdsListService().getAdsList(page, maxItems, sortKey);
            results.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new rx.Observer<GetAdsListResponseModel>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.onFetchDataError(e.getMessage());
                            }
                        }

                        @Override
                        public void onNext(GetAdsListResponseModel getAdsListResponseModel) {
                            if (view != null) {
                                view.onFetchDataSuccess(getAdsListResponseModel);
                            }
                        }
                    });
        }
    }

    @Override
    public void subscribe(int page, int maxItems, String sortKey) {
        loadData(page, maxItems, sortKey);
    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
